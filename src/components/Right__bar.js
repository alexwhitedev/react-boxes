import React from 'react';


class RightBar extends React.Component {
    render() {
        return(
            <div className="right__bar">
                <div className="score__b">
                    SCORE B:
                    <div className="">
                        {this.props.score}
                    </div>
                </div>
            </div>
        );
    }
}

export default RightBar;