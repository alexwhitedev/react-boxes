import React, { Component } from "react";


import '../styles/App.css';
import '../styles/Game.css';
import '../styles/normalize.css';

import Header from "./Header";
import Main from "./Main";
import Footer from "./Footer";


class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            player: 'A'
        };
        this.updatePlayer = this.updatePlayer.bind(this);
    }
    updatePlayer(pl){
        this.state.player = pl;
        this.setState({player: pl})
        // console.log(this.state.player);

    }
    render() {
        return (
            <div className="wrapper">
                <Header player={this.state.player}/>
                <Main updatePlayer = {this.updatePlayer}/>
                <Footer/>
            </div>
        );
    }
}

export default App;