import React from 'react';

import Dot from './Dots';
import Line from './Lines';
import Letter from './Letters';
import { SSL_OP_NO_SESSION_RESUMPTION_ON_RENEGOTIATION } from 'constants';

class Game extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            isOpen: 'not open',
            row1: 0,
            col1: 0,
            row2: 0,
            col2: 0
        }

        this.statePlayer = {
            nowPlay: 'A',
            scoreA: this.props.scoreNowA,
            scoreB: this.props.scoreNowB
        }

        this.stateLines = {
            isVisibleh00_01: 'hidden',
            isVisibleh01_02: 'hidden',
            isVisibleh10_11: 'hidden',
            isVisibleh11_12: 'hidden',
            isVisibleh20_21: 'hidden',
            isVisibleh21_22: 'hidden',

            isVisiblev00_10: 'hidden',
            isVisiblev01_11: 'hidden',
            isVisiblev02_12: 'hidden',
            isVisiblev10_20: 'hidden',
            isVisiblev11_21: 'hidden',
            isVisiblev12_22: 'hidden',

            colorh00_01: 'red',
            colorh01_02: 'red',
            colorh10_11: 'red',
            colorh11_12: 'red',
            colorh20_21: 'red',
            colorh21_22: 'red',

            colorv00_10: 'red',
            colorv01_11: 'red',
            colorv02_12: 'red',
            colorv10_20: 'red',
            colorv11_21: 'red',
            colorv12_22: 'red'
        }

        this.stateLetters = {
            isPlayer0: '',
            isPlayer1: '',
            isPlayer2: '',
            isPlayer3: ''
        }

        this.updateDataDots = this.updateDataDots.bind(this);
        this.logic = this.logic.bind(this);    
    };

    updateDataDots(value, row, col){
        if(this.state.isOpen == 'not open' || this.state.isOpen == 'closed'){
            this.setState({row1: row, col1: col});
            this.state.row1 = row;
            this.state.col1 = col;
        }

        if(this.state.isOpen == 'open'){
            
            this.setState({row2: row, col2: col});
            this.state.row2 = row;
            this.state.col2 = col;

            if(this.state.row1 == this.state.row2){
                var key = ''
                if(this.state.col1 == this.state.col2 - 1){
                    key = 'isVisible' + 'h' + this.state.row1 + this.state.col1 + '_' + this.state.row1 + this.state.col2;
                    this.stateLines[key] = '';
                }
                if(this.state.col2 == this.state.col1 - 1){
                    key = 'isVisible' + 'h' + this.state.row2 + this.state.col2 + '_' + this.state.row1 + this.state.col1;
                    this.stateLines[key] = '';
                }
                // logic(this.stateLines, key);                
            }

            if(this.state.col1 == this.state.col2){
                var key = '';
                if(this.state.row1 == this.state.row2 - 1){
                    key = 'isVisible' + 'v' + this.state.row1 + this.state.col1 + '_' + this.state.row2 + this.state.col2;
                    this.stateLines[key] = '';
                }
                if(this.state.row2 == this.state.row1 - 1){
                    key = 'isVisible' + 'v' + this.state.row2 + this.state.col2 + '_' + this.state.row1 + this.state.col1;
                    this.stateLines[key] = '';
                }
                // logic(this.stateLines, key)
            }
            this.logic();
            if(this.statePlayer.nowPlay == 'A'){
                this.statePlayer.nowPlay = 'B';
                // console.log(this.statePlayer.nowPlay);
                this.props.updatePlayer(this.statePlayer.nowPlay);          
            } else {
                this.statePlayer.nowPlay = 'A';
                // console.log(this.statePlayer.nowPlay);

                this.props.updatePlayer(this.statePlayer.nowPlay);          
            }
        }

        this.state.isOpen = value;
        this.setState({isOpen: value});
        // console.log(this.state);
    };

    logic(){

        if(this.stateLines.isVisibleh00_01 == '' && this.stateLines.isVisibleh10_11 == ''
                    && this.stateLines.isVisiblev00_10 == '' && this.stateLines.isVisiblev01_11 == '' 
                    && this.stateLetters.isPlayer0 == ''){
            this.stateLetters.isPlayer0 = this.statePlayer.nowPlay;

            if(this.statePlayer.nowPlay == 'A'){

                this.statePlayer.scoreA = '' + (+this.statePlayer.scoreA + 1);

                this.statePlayer.nowPlay = 'B';            
            } 
            else {

                this.statePlayer.scoreB = '' + (+this.statePlayer.scoreB + 1);                

                this.statePlayer.nowPlay = 'A';
            }
            this.props.updateScore(this.statePlayer.scoreA, this.statePlayer.scoreB);

        }


        if(this.stateLines.isVisibleh10_11 == '' && this.stateLines.isVisibleh20_21 == ''
                    && this.stateLines.isVisiblev10_20 == '' && this.stateLines.isVisiblev11_21 == '' 
                    && this.stateLetters.isPlayer1 == ''){
            this.stateLetters.isPlayer1 = this.statePlayer.nowPlay;
            
            if(this.statePlayer.nowPlay == 'A'){

                this.statePlayer.scoreA = '' + (+this.statePlayer.scoreA + 1);

                this.statePlayer.nowPlay = 'B';            
            } 
            else {

                this.statePlayer.scoreB = '' + (+this.statePlayer.scoreB + 1);                
                // console.log(this.statePlayer.scoreB);

                this.statePlayer.nowPlay = 'A';
            }
            this.props.updateScore(this.statePlayer.scoreA, this.statePlayer.scoreB);

        }


        if(this.stateLines.isVisibleh01_02 == '' && this.stateLines.isVisibleh11_12 == ''
                    && this.stateLines.isVisiblev01_11 == '' && this.stateLines.isVisiblev02_12 == '' 
                    && this.stateLetters.isPlayer2 == ''){
            this.stateLetters.isPlayer2 = this.statePlayer.nowPlay;
            
            if(this.statePlayer.nowPlay == 'A'){

                this.statePlayer.scoreA = '' + (+this.statePlayer.scoreA + 1);

                this.statePlayer.nowPlay = 'B';            
            } 
            else {

                this.statePlayer.scoreB = '' + (+this.statePlayer.scoreB + 1);                
                // console.log(this.statePlayer.scoreB);

                this.statePlayer.nowPlay = 'A';
            }
            this.props.updateScore(this.statePlayer.scoreA, this.statePlayer.scoreB);

        }


        if(this.stateLines.isVisibleh11_12 == '' && this.stateLines.isVisibleh21_22 == ''
                    && this.stateLines.isVisiblev11_21 == '' && this.stateLines.isVisiblev12_22 == '' 
                    && this.stateLetters.isPlayer3 == ''){
            this.stateLetters.isPlayer3 = this.statePlayer.nowPlay;
            
            if(this.statePlayer.nowPlay == 'A'){

                this.statePlayer.scoreA = '' + (+this.statePlayer.scoreA + 1);
                this.statePlayer.nowPlay = 'B';            
            } 
            else {

                this.statePlayer.scoreB = '' + (+this.statePlayer.scoreB + 1);                
                // console.log(this.statePlayer.scoreB);

                this.statePlayer.nowPlay = 'A';
            }
            this.props.updateScore(this.statePlayer.scoreA, this.statePlayer.scoreB);


        }
        
    };
    
    

    render() {
        return(
            <div className="game">

                <Line lineKey = {'h00_01'} x1={0} y1={5} x2={110} y2={5} isVisible = {this.stateLines.isVisibleh00_01} color = {this.stateLines.colorh00_01}/>
                <Line lineKey = {'h10_11'} x1={0} y1={115} x2={110} y2={115} isVisible = {this.stateLines.isVisibleh10_11} color = {this.stateLines.colorh10_11}/>
                <Line lineKey = {'h20_21'} x1={0} y1={225} x2={110} y2={225} isVisible = {this.stateLines.isVisibleh20_21} color = {this.stateLines.colorh20_21}/>
                <Line lineKey = {'h01_02'} x1={110} y1={5} x2={220} y2={5} isVisible = {this.stateLines.isVisibleh01_02} color = {this.stateLines.colorh01_02}/>
                <Line lineKey = {'h11_12'} x1={110} y1={115} x2={220} y2={115} isVisible = {this.stateLines.isVisibleh11_12} color = {this.stateLines.colorh11_12}/>
                <Line lineKey = {'h21_22'} x1={110} y1={225} x2={220} y2={225} isVisible = {this.stateLines.isVisibleh21_22} color = {this.stateLines.colorh21_22}/>
                
                <Line lineKey = {'v00_10'} x1={5} y1={0} x2={5} y2={110} isVisible = {this.stateLines.isVisiblev00_10} color = {this.stateLines.colorv00_10}/>
                <Line lineKey = {'v01_11'} x1={115} y1={0} x2={115} y2={110} isVisible = {this.stateLines.isVisiblev01_11} color = {this.stateLines.colorv01_11}/>
                <Line lineKey = {'v02_12'} x1={225} y1={0} x2={225} y2={110} isVisible = {this.stateLines.isVisiblev02_12} color = {this.stateLines.colorv02_12}/>
                <Line lineKey = {'v10_20'} x1={5} y1={110} x2={5} y2={220} isVisible = {this.stateLines.isVisiblev10_20} color = {this.stateLines.colorv10_20}/>
                <Line lineKey = {'v11_21'} x1={115} y1={110} x2={115} y2={220} isVisible = {this.stateLines.isVisiblev11_21} color = {this.stateLines.colorv11_21}/>
                <Line lineKey = {'v12_22'} x1={225} y1={110} x2={225} y2={220} isVisible = {this.stateLines.isVisiblev12_22} color = {this.stateLines.colorv12_22}/>

                <Letter player = {this.stateLetters.isPlayer0} x={60} y = {60}/>
                <Letter player = {this.stateLetters.isPlayer1} x={60} y = {180}/>
                <Letter player = {this.stateLetters.isPlayer2} x={180} y = {60}/>
                <Letter player = {this.stateLetters.isPlayer3} x={180} y = {180}/>
                

                <Dot row = '0' col = '0' updateDataDots = {this.updateDataDots} isDotOpen = {this.state.isOpen}/>
                <Dot row = '0' col = '1' updateDataDots = {this.updateDataDots} isDotOpen = {this.state.isOpen}/>
                <Dot row = '0' col = '2' updateDataDots = {this.updateDataDots} isDotOpen = {this.state.isOpen}/>
                <Dot row = '1' col = '0' updateDataDots = {this.updateDataDots} isDotOpen = {this.state.isOpen}/>
                <Dot row = '1' col = '1' updateDataDots = {this.updateDataDots} isDotOpen = {this.state.isOpen}/>
                <Dot row = '1' col = '2' updateDataDots = {this.updateDataDots} isDotOpen = {this.state.isOpen}/>
                <Dot row = '2' col = '0' updateDataDots = {this.updateDataDots} isDotOpen = {this.state.isOpen}/>
                <Dot row = '2' col = '1' updateDataDots = {this.updateDataDots} isDotOpen = {this.state.isOpen}/>
                <Dot row = '2' col = '2' updateDataDots = {this.updateDataDots} isDotOpen = {this.state.isOpen}/>
            
            </div>
        );
    }
}

// function logic(state, key){
//     var buffLine1 = '' + key[10] + key[11];
//     var buffLine2 = '' + key[13] + key[14];
//     // console.log(buffLine1, buffLine2);
//     // buffLine2 = '' + (+buffLine2 - 1);
//     // console.log(buffLine2);
//     if 
// }

export default Game;