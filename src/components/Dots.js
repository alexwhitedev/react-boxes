import React from 'react';


class Dot extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            isOpen: 'not open'
        }

        this.dotClick = this.dotClick.bind(this)
        this.selector = React.createRef(this);
    }

    dotClick(){
        // console.log(this.props.isDotOpen);
        if (this.props.isDotOpen == 'not open' || this.props.isDotOpen == 'closed'){
            // console.log('DOT WAS OPENED');
            this.props.updateDataDots('open', this.props.row, this.props.col);
        }

        if (this.props.isDotOpen == 'open'){
            // console.log('DOT WAS CLOSED');
            this.props.updateDataDots('closed', this.props.row, this.props.col);
        }
    }

    render() {        
        return(
            <div ref={this.selector} className = {"dot" + this.props.row + this.props.col + " dot"} onClick = {this.dotClick}></div>
        );
    }
}

export default Dot;