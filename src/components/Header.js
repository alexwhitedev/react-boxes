import React from 'react';

// import '../styles/App.css';

class Header extends React.Component {
    render() {
        return(
            <div className="header">
                <div className="header__text">
                    Хiд гравця: {this.props.player}
                </div>
            </div>
        );
    }
}

export default Header;