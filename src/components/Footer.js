import React from 'react';


class Footer extends React.Component {
    render() {
        return(
            <div className="footer">
                <div className="footer__text">
                    developed by Oleksandr Bilykh
                </div>
            </div>
        );
    }
}

export default Footer;