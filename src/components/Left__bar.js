import React from 'react';


class LeftBar extends React.Component {
    render() {
        return(
            <div className="left__bar">
                <div className="score__a">
                    SCORE A:
                    <div className="">
                        {this.props.score}
                    </div>
                </div>
            </div>
        );
    }
}

export default LeftBar;