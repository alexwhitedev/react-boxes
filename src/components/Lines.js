import React from 'react';


class Line extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            isView: false
        }
    }
    render() {
        return(
            <svg>

                <line className = {this.props.lineKey} x1={this.props.x1} y1={this.props.y1} x2={this.props.x2} y2={this.props.y2} 
                    style={{stroke: this.props.color, strokeWidth: 5, visibility: this.props.isVisible}} />

            </svg>
        );
    }
}

export default Line;