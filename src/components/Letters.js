import React from 'react';


class Letter extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
        }

    }
    render() {
        return(            
            <svg className="player__letter">
                <text x={this.props.x} y={this.props.y} fill="red" fontSize="50">{this.props.player}</text>
            </svg>
        );
    }
}

function dotClick() {
    // console.log('clicked')
}

export default Letter;