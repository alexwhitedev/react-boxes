import React from 'react';


import LeftBar from './Left__bar';
import RightBar from './Right__bar';
import Game from './Game';


// import '../styles/App.css';

class Main extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            scoreA: '0',
            scoreB: '0',
            playerNow: 'A'
        }
    
        this.updateScore = this.updateScore.bind(this);
        this.updatePlayer = this.updatePlayer.bind(this);
    }

    updateScore(scA, scB){
        this.state.scoreA = scA;
        this.setState({scoreA: scA})
        this.state.scoreB = scB;
        this.setState({scoreB: scB})
        // console.log(this.state.scoreA);
        // console.log(this.state.scoreB);
    };

    updatePlayer(player){
        this.state.playerNow = player;
        this.setState({playerNow: player});
        this.props.updatePlayer(this.state.playerNow);
    };


    render() {
        return(
            <div className="main">
                <LeftBar score = {this.state.scoreA}/>
                <Game updateScore = {this.updateScore} updatePlayer = {this.updatePlayer} scoreNowA = {this.state.scoreA} scoreNowB = {this.state.scoreB}/>
                <RightBar score = {this.state.scoreB}/>
            </div>
        );
    }
}

export default Main;